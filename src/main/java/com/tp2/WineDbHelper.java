package com.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localisation";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "plantedArea";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE cellar (_id REAL, name TEXT, region TEXT, localisation TEXT, climate TEXT, plantedArea TEXT, UNIQUE(" + COLUMN_NAME + ", " + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        String nom = wine.getTitle();
        String region = wine.getRegion();
        String localisation = wine.getLocalization();
        String climate = wine.getClimate();
        String plantedArea = wine.getPlantedArea();

        ContentValues cv=new ContentValues();
        cv.put(COLUMN_NAME, nom);
        cv.put(COLUMN_WINE_REGION, region);
        cv.put(COLUMN_LOC, localisation);
        cv.put(COLUMN_CLIMATE, climate);
        cv.put(COLUMN_PLANTED_AREA, plantedArea);

        // Inserting Row
        SQLiteDatabase db = this.getWritableDatabase();
        long rowID = db.insert(TABLE_NAME, COLUMN_NAME, cv);
        // call db.insert()

        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = 0; //TMP

        // updating row
        // call db.update()
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[] { _ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA },
                null, null, null, null, null);
        cursor.moveToLast();

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        System.out.println("del "+cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME)));
        // call db.delete();
        db.delete(TABLE_NAME,"name=?", new String[]{cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME))});
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        System.out.println(cursor);
        String nom = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME));
        String region = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION));
        String localisation = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC));
        String climate = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_CLIMATE));
        String plantedArea = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_PLANTED_AREA));


        Wine wine = new Wine(nom,region,localisation,climate,plantedArea);
        return wine;
    }
}
