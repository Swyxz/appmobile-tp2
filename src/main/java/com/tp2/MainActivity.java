package com.tp2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

//import androidx.appcompat.app.AppCompatActivity;
import android.support.design.widget.FloatingActionButton;
//import com.google.android.material.floatingactionbutton.FloatingActionButton;
//import com.google.android.material.snackbar.Snackbar;

import java.io.File;

import static android.widget.ListPopupWindow.MATCH_PARENT;
import static android.widget.ListPopupWindow.WRAP_CONTENT;


public class MainActivity extends AppCompatActivity {
    protected View selectedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File file = new File("/data/data/com.tp2/databases/wine.db");
        WineDbHelper dbHelper;
        SQLiteDatabase db;
        if(!file.exists()) {
            dbHelper = new WineDbHelper(this);
            db = dbHelper.getReadableDatabase();
            dbHelper.populate();
        }else{
            dbHelper = new WineDbHelper(this);
            db = dbHelper.getReadableDatabase();
        }
        // Close the database
        db.close();

        final ListView listview = (ListView) findViewById(R.id.lstView);
        registerForContextMenu(listview);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(v.getContext(), WineActivity.class);
                Bundle b = new Bundle();
                intent.putExtra("nom", b);
                Cursor z = (Cursor) parent.getItemAtPosition(position);
                String t =z.getString(z.getColumnIndex("name"));
                b.putString("nom", t);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), WineActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View vue, ContextMenu.ContextMenuInfo menuInfo) {
        this.selectedView = vue;
        super.onCreateContextMenu(menu, vue, menuInfo);
        menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "Supprimer cet élément");
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final ListView ls = (ListView) findViewById(R.id.lstView);
        Cursor z = (Cursor) ls.getItemAtPosition(info.position);
        switch (item.getItemId()) {
            case Menu.FIRST:
                WineDbHelper dbHelper = new WineDbHelper(this);
                dbHelper.deleteWine(z);
                finish();
                startActivity(getIntent());
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        WineDbHelper dbHelper = new WineDbHelper(this);
        final ListView listview = findViewById(R.id.lstView);
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, dbHelper.fetchAllWines(), new String[] { WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION }, new int[] { android.R.id.text1, android.R.id.text2 }, 0);
        listview.setAdapter(adapter);
    }
}