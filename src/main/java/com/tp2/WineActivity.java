package com.tp2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//import androidx.appcompat.app.AppCompatActivity;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("nom");
        if (bundle != null) {
            WineDbHelper dbHelper = new WineDbHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor current = db.query(dbHelper.TABLE_NAME, new String[]{dbHelper._ID, dbHelper.COLUMN_NAME, dbHelper.COLUMN_WINE_REGION, dbHelper.COLUMN_LOC, dbHelper.COLUMN_CLIMATE, dbHelper.COLUMN_PLANTED_AREA},
                    "name=?", new String[]{bundle.getString("nom")}, null, null, null);
            current.moveToFirst();

            final TextView name = (TextView) findViewById(R.id.wineName);
            name.setText(current.getString(current.getColumnIndex(WineDbHelper.COLUMN_NAME)));

            final EditText region = (EditText) findViewById(R.id.editWineRegion);
            region.setText(current.getString(current.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION)));

            final EditText localisation = (EditText) findViewById(R.id.editLoc);
            localisation.setText(current.getString(current.getColumnIndex(WineDbHelper.COLUMN_LOC)));

            final EditText climat = (EditText) findViewById(R.id.editClimate);
            climat.setText(current.getString(current.getColumnIndex(WineDbHelper.COLUMN_CLIMATE)));

            final EditText plantedArea = (EditText) findViewById(R.id.editPlantedArea);
            plantedArea.setText(current.getString(current.getColumnIndex(WineDbHelper.COLUMN_PLANTED_AREA)));
        }

    }

    public void saveStat(View v){
        final TextView name = (TextView) findViewById(R.id.wineName);
        final String n = ""+ name.getText();

        final EditText region = (EditText) findViewById(R.id.editWineRegion);
        final String re = ""+ region.getText();

        final EditText localisation = (EditText) findViewById(R.id.editLoc);
        final String lo = ""+ localisation.getText();

        final EditText climat = (EditText) findViewById(R.id.editClimate);
        final String cl = ""+ climat.getText();

        final EditText plantedArea = (EditText) findViewById(R.id.editPlantedArea);
        final String pl = ""+ plantedArea.getText();

        WineDbHelper dbHelper = new WineDbHelper(this);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor current = db.query(WineDbHelper.TABLE_NAME, new String[] {WineDbHelper.COLUMN_NAME},
                "name=? and region=?", new String[] {n,re}, null, null, null);

        if (n.isEmpty()) {
            new AlertDialog.Builder(this)
                    .setTitle("Sauvegarde impossible")
                    .setMessage("Le nom du vin doit être renseigné.")
                    .show();
        }else {
            if (current != null && current.moveToFirst()) {
                new AlertDialog.Builder(this)
                        .setTitle("Attention !")
                        .setMessage("Vous êtes sur le point de modifier un vin existant, continuer ?")
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ContentValues cv = new ContentValues();
                                cv.put(WineDbHelper.COLUMN_NAME, n);
                                cv.put(WineDbHelper.COLUMN_WINE_REGION, re);
                                cv.put(WineDbHelper.COLUMN_LOC, lo);
                                cv.put(WineDbHelper.COLUMN_CLIMATE, cl);
                                cv.put(WineDbHelper.COLUMN_PLANTED_AREA, pl);
                                db.update(WineDbHelper.TABLE_NAME, cv, "name=? and region=?", new String[]{n,re});
                                finish();
                            }
                        })
                        .setNegativeButton("Non", null)
                        .show();
            } else {
                dbHelper.addWine(new Wine(n, re, lo, cl, pl));
                finish();
            }
        }
    }
}